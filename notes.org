* Motivation
  - No more confusion about version numbers
  - Being always able to revert changes
  - Collaborators will always have latest version
  - Decentralize your research. Work wherever you want
  - Online backup. Every single change will the saved
  - Storing research. We are required to save our research. With the
    tools I present to you can both keep them in one place quite
    conveniently and run the analysis even a couple of years
    later. (Which most probably won't even be possible on our
    workstations).


* Git
** General
** Use-cases 
   - Writing a manuscript -> multiple revisions
   - Numerical analysis -> different versions of code and results
   - Giving a talk -> which version of my code I took the figures from?
   - Collecting your software in packages -> didn't this worked at one
     point in time?
   - Taking notes -> never loose a single note

   Maybe not-use-cases:
   - Writing emails
   - Searchbar in browser
** Basic commands
   - clone
   - pull
   - add
   - commit
   - push

** Submodules for bigger projects
** What is HEAD and origin?
** Show examples of squashing, rebase, and merging.

   If you dig deeper, it's a fairly complex tool.

* RMarkdown | Jupyter | Org-mode
    We just established *git* is the best tool ever. So, why do we
    need yet another framework?

    - Split text and codes and keep them under version control
      separately
    - Instead of new PDF, git just saves a diff in the heading you
      altered -> keeps projects at comparable size.
    - Each time you want to produce the current version you compile it
      from the source -> caching for faster compilation (whats caching?)
    - Big benefit: automatic integration of figures. No version
      control of figures anymore! 
    - Global variables for figures. Changing one value -> affecting
      	      <li><b>docker </b></li>

      all figures. Use the setup of one project for the next one.

    Pandoc
    - Write text in Markdown. Faster than LaTeX.
    - Use Pandoc to convert it into PDF or HTML -> PDF Book and
      Gitbook using exactly the same underlying text and script

    Continuous integration
    - Do we have to the code every time ourselves? How to edit the
      documents online when not seen the final result?
    - Virtual machine on a server performing commands every time you
      push to a specific branch of your repository
    - It will compile text and data, gather all produced artifacts,
      and displays it in a convenient way

    For free? Of course not! But you can use my setup to provide
    reveal.js talks or RMarkdown documents. .gitlab-ci.yml


* Docker
  
  Your setup, your code, your packages. They all might change in time
  and break the compilation.

  Using Docker you can freeze dependencies and content into an image
  of an operation system. This can be used to run your code some years
  in the future or it can be used the continuous integration of our
  Gitlab server. There, all commands are run in your Docker image and
  the code just keeps on working.

  Packages and OS will grew old. Lack of security? Nope, just a module
  performing an operation and providing the resulting document. No
  additional interactions.

  No full virtual machine. Like git for operation systems. Get an
  account at hub.docker.com for free and host as many images as you
  want.


** Creating a custom environment

   Example starting from Ubuntu16, installing some dependencies and R
   packages and providing the results with an R prompt.

* Combine all ingredients of Travis | Gitlab CI
  - Compiling a manuscript, hosting a talk, show latest results of a
    collaboration
    
* Problems
  I'm not a salesperson for one of those products. There are problems.
  - You need to use the tools properly -> invest time to learn the basics
  - Without sufficient documentation you won't find anything in your
    git history
  - If you delve deeper, git is a rather complex tool. But when being
    the only person working on a repository you always never need more
    advanced commands than the ones presented in this talk.
  - The more ingredients in your tool chain, the more likely things
    will break in time. But I think it's worth the effort.
  
-

Talk about how to perform reproducible research on the example of my
PhD thesis.

# Deployment

### Gitlab Pages

To access the presentation rendered using
[reveal.js](https://github.com/hakimel/reveal.js), please visit the
corresponding Gitlab Page
http://phmu.io.pks.mpg.de/talk-reproducible-research/index.html

See the [.gitlab-ci.yml](.gitlab-ci.yml) file for deployment details.

### Local

If you, instead, want to *serve* this presentation *locally* (it will be
updated anytime you save file contained in the presentation), firstly,
initialize all submodules by

> git submodule update --init --recursive --remote

Afterwards, you need to install all requirements of
[grunt](https://gruntjs.com/) to be able to watch your files,
transform all your .scss and .sass files into .css etc. pp.

> npm install

Finally, you can serve the presentation by executing one of the
following commands 

> npm start

or 

> grunt serve


# Figures and styling

The [talk.css](css/talk.css) contains some custom additions to the CSS
styling of the overall reveal.js presentation.

# Copyright

All figures inside of the talk I do not hold the copyright of are
labelled appropriately.
